$(function () { 
	// Установка активного пункта меню
	var route = location.pathname.split("/")[1];
	if (route == '' || route == 'index' || route == 'index.php') {
		route = 'main';
	}
	$('.main-menu a[href^="/' + route + '"]').closest('li').addClass('active');
	
});

$(document).on('ajaxStart', function(){
	$('#ajaxloader').show();
}).on('ajaxStop', function (){
	$('#ajaxloader').hide();
});
