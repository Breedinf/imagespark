function refreshGrid(){
	$.ajax({
		url: '/results/get_data',
		type: 'post',
		dataType: 'json',
		data: {},
		success: function (data) {
			var categories = data.categories;
			categories.unshift({id: '0', name: ''});
			var brands = data.brands;
			brands.unshift({id: '0', name: ''});
			
			$("#jsGrid").jsGrid({
					width: "100%",
					/*height: "850px",*/

					inserting: false,
					deleting: false,
					editing: false,
					sorting: true,
					paging: true,
					filtering: true,
					
					data: data.products,

					pagerFormat: "Страницы: {first} {prev} {pages} {next} {last} &nbsp;&nbsp; Всего {itemCount} результатов.",

					fields: [
						{ name: "category_id", type: "select", items: categories, valueField: "id", textField: "name", title: "Категория", autosearch: true},
						{ name: "brand_id", type: "select", items: brands, valueField: "id", textField: "name", title: "Бренд"},
						{ name: "name", type: "text", width: 200, title: "Название", autosearch: true},
						{ name: "model", type: "text", title: "Модель"},
						{ name: "article", type: "text", title: "Артикул" },
						{ name: "sizes", type: "text", title: "Размеры"},
						{ name: "colors", type: "text", title: "Цвета"},
						{ name: "side", type: "text", title: "Ориентация"},
					], 
					
					controller: {
						loadData: function (filter) {
							return $.grep(data.products, function(item) {
								var success = true;
								if ((filter.name && item.name.indexOf(filter.name) == -1) ||
									(filter.category_id != "0" && item.category_id != filter.category_id) ||
									(filter.brand_id != "0" && item.brand_id != filter.brand_id) ||
									(filter.article && item.article.indexOf(filter.article) == -1) ||
									(filter.model && item.model.indexOf(filter.model) == -1) ||
									(filter.colors && item.colors.indexOf(filter.colors) == -1) ||
									(filter.sizes && item.sizes.indexOf(filter.sizes) == -1) ||
									(filter.side && item.side.indexOf(filter.side) == -1))
									success = false;
								return success;
							});
						},
					}
				});
		}
	})
}

$(document).ready(function (){
	refreshGrid();
});

