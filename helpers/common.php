<?php

if(!function_exists('mb_ucfirst')){
	function mb_ucfirst($string, $enc = 'UTF-8') {
		return mb_strtoupper(mb_substr($string, 0, 1, $enc), $enc) . 
			mb_substr($string, 1, mb_strlen($string, $enc), $enc);
	}
}

if (!function_exists('mb_stripos_multiple')) {
	function mb_stripos_multiple($haystack, $needle_array) {
		foreach ($needle_array as $needle) {
			$res = mb_stripos($haystack, $needle);
			if ($res !== false) {
				return $res;
			}
		}
		return false;
	}
}

if (!function_exists('mb_trim')) {
	function mb_trim( $string ) { 
		$string = preg_replace( "/(^\s+)|(\s+$)/us", "", $string ); 
		return $string; 
	}
}