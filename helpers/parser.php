<?php

class Parser {
	public $brands = ["pallas", "ccm", "reebok", "bauer", "tackla", "torspo", "nbh", "montreal", "easton", "jofa", "oakley", "tps", "sher-wood", "tac", "kosa", "graf", "rbk", "koho", "vegum", "viking", "grit"];
	public $categories = ["Сумки", "Клюшки", "Коньки", "Защита", "Одежда", "Разное"];
	
	private function getCategory($string) {
		if (mb_stripos($string, "сумка") !== false) {
			return "Сумки";
		}
		
		if (preg_match('/(клюшк)/usi', $string)) {
			return "Клюшки";
		}
		
		if (mb_stripos($string, "коньки") !== false) {
			return "Коньки";
		}

		if (mb_stripos_multiple($string, ["защита", "раковина", "налокотник", "нагрудник", "маска", "шлем", "визор", "щитки"]) !== false) {
			return "Защита";
		}

		if (mb_stripos_multiple($string, ["трусы", "брюки", "шорты", "перчатки", "белье", "гамаши", "форма", "толстовка", "кофта", "свитер", "куртка", "майка", "футболка", "костюм"]) !== false) {
			return "Одежда";
		}
		
		return "Разное";

	}

	public function parse($row) {
		$result = [
			"brand" => null, 
			"article" => null, 
			"size" => null, 
			"side" => null, 
			"color" => null, 
			"model" => null,
			"category" => null,
			"name" => ''
		];
		
		$str = iconv("cp1251", "utf-8", $row[0]);
			
		// определяем бренд (поиск из перечня)
		foreach ($this->brands as $brand) {
			$index = mb_stripos($str, $brand);
			if ($index !== false) {
				$result["brand"] = $brand;
				$str = mb_substr($str, 0, $index) . mb_substr($str, $index + strlen($brand) + 1);
				break;
			}
		}

		// определяем артикул
		$article_pattern = '/\(?H?[0-9]{7,9}\s?\)?/';
		$article = preg_match($article_pattern, $str, $matches);
		if ($article) {
			$result["article"] = preg_replace('/[\(\)]/', '', $matches[0]);
			$str = preg_replace($article_pattern, '', $str);
		}

		// определяем категорию (из перечня)
		$result["category"] = $this->getCategory($str);

		// определяем размеры или ориентацию клюшки ( то, что после дефиса )
		$sizes_start_index = mb_strpos($str, ' - ');
		if ($sizes_start_index !== false) {
			$key = ($result["category"] == "Клюшки") ? "side" : "size";
			$result[$key] = preg_replace('/(-\s)/', '', mb_substr($str, $sizes_start_index + 1));
			$str = mb_substr($str, 0, $sizes_start_index);
		}

		// Определяем модель (всё, что не русские буквы)
		$model_pattern = '/\s?([^а-я\s]+\-*[^а-я\s]+|[^а-я\s]+)/usi';
		$model = preg_match_all($model_pattern, $str, $matches);
		if ($model) {
			foreach ($matches[0] as $match) {
				$result["model"] .= $match . ' ';
			}
			$str = preg_replace($model_pattern, '', $str);
		}

		// Из остатка достаем цвет
		$color_pattern = '/(\s|\(){1}[а-я.\-\/ё]*(белы|чер|крас|син|желт|зол|сер|зелен)[а-я.\-\/ё]*(\s|\)|$)+/usi';
		$color = preg_match($color_pattern, $str, $matches);
		if ($color) {
			$result["color"] = $matches[0];
			$str = preg_replace($color_pattern, '', $str);
		}

		// То, что осталось - название товара
		$result["name"] = $str;
		
		
		foreach ($result as &$value) {
			$value = mb_trim($value);
		}
		
		if (empty($result["name"])) {
			$result["name"] = $result["model"];
		}
		
		return $result;
	}
	
}
