<?php

class ResultsController extends Controller
{
	function action_index(){	
		
		return $this->view->render('results');
	}
	
	function action_get_data() {
		$products_model = new ProductsModel();
		$products = $products_model->findAll();
		
		$categories_model = new CategoriesModel();
		$categories = $categories_model->findAll();
		
		$brands_model = new BrandsModel();
		$brands = $brands_model->findAll();
		
		return json_encode([
			"categories" => $categories, 
			"brands" => $brands, 
			"products" => $products
		]);
	}
}