<?php

require_once '/helpers/csv.php';
require_once '/helpers/parser.php';

class MainController extends Controller {
	
	function action_index(){	
		return $this->view->render('main');
	}
	
	function action_import($data) {
		$parser = new Parser();
		
		$brands_model = new BrandsModel();
		$brands_model->deleteAll();
		$brands_ids = [];
		foreach ($parser->brands as $brand) {
			$brands_ids[$brand] = $brands_model->insert(["name" => $brand]);
		}
		
		$categories_model = new CategoriesModel();
		$categories_model->deleteAll();
		$categories_ids = [];
		foreach ($parser->categories as $category) {
			$categories_ids[$category] = $categories_model->insert(["name" => $category]);
		}
		
		$csv = new CSV($_FILES['file']['tmp_name']);
		$data = $csv->getCSV();
		
		$products_model = new ProductsModel();
		$products_model->deleteAll();
		foreach ($data as $row) {
			$parsed_row = $parser->parse($row);
			$products_model->insert([
				"name" => $parsed_row["name"],
				"article" => $parsed_row["article"], 
				"category_id" => $categories_ids[$parsed_row["category"]], 
				"brand_id" => $brands_ids[$parsed_row["brand"]], 
				"colors" => $parsed_row["color"], 
				"sizes" => $parsed_row["size"], 
				"side" => $parsed_row["side"],
				"model" => $parsed_row["model"]
			]);
		}
		
		return $this->redirect('results');
	}
	
}