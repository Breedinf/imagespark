<!DOCTYPE html>
<html lang="ru">
<head>
	<meta charset="utf-8">
	<title>Главная</title>
	<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
	<link rel="stylesheet" type="text/css" href="/css/jsGrid/jsgrid.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/jsGrid/jsgrid-theme.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/style.css" />
</head>
<body>
	<div class="wrapper">
		<?php include 'header.php'; ?>
		<div class="content container-fluid">
			<?php include 'application/views/'.$content_view.'.php'; ?>
		</div>
		<?php include 'footer.php'; ?>
	</div>
	<div id="ajaxloader">
		<img style="margin:0 auto" src="/images/loader.gif"></img>
	</div>
</div>	
</body>

<script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js" integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa" crossorigin="anonymous"></script>
<script src="/js/jsGrid/jsgrid.min.js"></script>
<script src="/js/common.js"></script>

<?php
	if (count($this->js) > 0) {
		foreach ($this->js as $js) {
			echo "<script src='$js'></script>";
		}
	}
?>

</html>