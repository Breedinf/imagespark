<?php

class View
{
	private $js = [];

	public function render($content_view, $data = null){
		if(is_array($data)) {
			extract($data);
		}
		
		include 'application/views/layouts/main.php';
	}
	
	public function renderAjax($content_view, $data = null) {
		if(is_array($data)) {
			extract($data);
		}
		
		include 'application/views/'.$content_view.'.php';
	}
	
	public function registerJsFile($src) {
		$this->js[] = $src;
	}
}