<?php

require_once '/application/models/load.php';

abstract class Model {
	abstract protected function getTableName();
	
	public function insert($params) {
		$db = App::getInstance()->db;
		$keys = implode(',', array_map(function ($item) {return '`'.$item.'`';}, array_keys($params)));
		$values = implode(',', array_map(function ($item) {return "'".$item."'";}, $params));

		$insert_query = "INSERT INTO `".$this->getTableName()."` ($keys) VALUES($values) ";
		
		$res = $db->query($insert_query);
		if ($res) {
			return $db->insert_id;
		}
		else {
			return false;
		}
	}
	
	public function select($where) {
		/* not implemented */
	}
	
	public function findById($id) {
		$db = App::getInstance()->db;
		$res = $db->query("SELECT * FROM `".$this->getTableName()."` WHERE `id` = ".$db->real_escape_string($id));
		if ($res) {
			return $res->fetch_assoc();
		}
		else {
			return false;
		}
	}
	
	public function findAll() {
		$res = App::getInstance()->db->query("SELECT * FROM `".$this->getTableName()."`");
		if ($res) {
			$result = [];
			while ($row = $res->fetch_assoc()) {
				$result[] = $row;
			}
			return $result;
		}
		else {
			return false;
		}
	}
	
	public function update($id, $params) {
		/* not implemented */
	}
	
	public function delete($id) {
		/* not implemented */
	}
	
	public function deleteAll() {
		return App::getInstance()->db->query("TRUNCATE TABLE `".$this->getTableName()."`");
	}
}

