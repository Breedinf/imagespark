<?php

class Router
{
	/*private function isAjaxRequest(){
        return isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest';
    }*/
	
	private function getRoute(&$controller, &$action, &$args) {
		$request = explode('?', $_SERVER['REQUEST_URI']);
        $route = (empty($request[0]) ? '' : $request[0]);
        
		unset($_SERVER['REQUEST_URI']);
        
		$controller = 'Main';
		$action = 'index';
		$args = $_REQUEST;
        
		$route = trim($route, '/\\');
		if (empty($route) || $route == 'index' || $route == 'index.php') {
			return;
        }
        else {
            $parts = explode('/', $route);
			
			if ( !empty($parts[0]) ){	
				$controller = $parts[0];
			}
			if ( !empty($parts[1]) ){
				$action = $parts[1];
			}
        }
    }
	
	public function start()	{
		$this->getRoute($controller_name, $action_name, $args);
		
		$controller_class = mb_ucfirst($controller_name).'Controller';
		$action = 'action_'.$action_name;

		$controller_file = "application/controllers/".$controller_class.'.php';
		if(file_exists($controller_file)){
			include $controller_file;
		}
		else{
			return $this->Page404();
		}			

		$controller = new $controller_class;
		
		if(method_exists($controller_class, $action))	{
			return $controller->$action($args);
		}
		else{
			return $this->Page404();
		}
	
	}
	
	public function Page404(){
        $host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.'notfound');
    }
}