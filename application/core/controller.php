<?php

abstract class Controller {

	public $view;
	
	function __construct(){
		$this->view = new View();
	}
	
	protected function redirect($url) {
		$host = 'http://'.$_SERVER['HTTP_HOST'].'/';
        header('HTTP/1.1 404 Not Found');
		header("Status: 404 Not Found");
		header('Location:'.$host.$url);
	}
}
