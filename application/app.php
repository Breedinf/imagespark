<?php

require_once 'config/db.php';
require_once 'core/model.php';
require_once 'core/view.php';
require_once 'core/controller.php';
require_once 'core/router.php';

require_once '/helpers/common.php';

class App 
{
	private static $app = null;
	public $db = null;

	private function __construct() {}
    private function __clone() {}
    private function __sleep() {}
    private function __wakeup() {}

	public static function getInstance() {
		 if (!(self::$app instanceof self)) {
            self::$app = new self();
        }
        return self::$app;
    }

	
	public function setup() {
		$this->db = new mysqli(HOST, DBUSER, DBPASSWORD, DBNAME);
		if (!$this->db) {
			die("Невозможно установить соединение c базой данных".$this->db->connect_errno());
		}
	}
	
	public function start() {
		$router = new Router();
		return $router->start();
	}
}

$app = App::getInstance();
$app->setup();
echo $app->start();